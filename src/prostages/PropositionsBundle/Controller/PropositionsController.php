<?php

namespace prostages\PropositionsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use prostages\PropositionsBundle\Entity\Entreprise;
use prostages\PropositionsBundle\Entity\Formation;
use prostages\PropositionsBundle\Entity\Stage;

class PropositionsController extends Controller
{
    public function indexAction()
    {
      $stages = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('prostagesPropositionsBundle:Stage')
        ->findAllStageFormationsEntreprise();

      //var_dump($stages);

        return $this->render('prostagesPropositionsBundle:Propositions:listeStages.html.twig',
      array('accueil' => true,
            'stages' => $stages));
    }

    public function entreprisesAction()
    {
      $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('prostagesPropositionsBundle:Entreprise');

      $entreprises = $repository->findAll();

      return $this->render('prostagesPropositionsBundle:Propositions:entreprises.html.twig',
          array('entreprises' => $entreprises));
    }

    public function formationsAction()
    {
      $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('prostagesPropositionsBundle:Formation');

      $formations = $repository->findAll();

      return $this->render('prostagesPropositionsBundle:Propositions:formations.html.twig',
          array('formations' => $formations));
    }

    public function triEntreprisesAction($id)
    {
      $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('prostagesPropositionsBundle:Stage');

      $stages = $repository->findAllStageByEntreprise($id);

      return $this->render('prostagesPropositionsBundle:Propositions:listeStages.html.twig',
              array('accueil' => false,
                'stages' => $stages));
    }

    public function triFormationsAction($id)
    {
      $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('prostagesPropositionsBundle:Stage');

      $stages = $repository->findAllStageByFormation($id);//find by formation

      return $this->render('prostagesPropositionsBundle:Propositions:listeStages.html.twig',
          array('accueil' => false,
            'stages' => $stages));
    }

    public function stageAction($id)
    {
      $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('prostagesPropositionsBundle:Stage');

      $stage = $repository->find($id);

      //var_dump($stage);

      return $this->render('prostagesPropositionsBundle:Propositions:stage.html.twig',
        array('stage' => $stage));
    }

    public function ajouterEntrepriseAction(Request $request) {

      $log['etat'] = null;
      $log['texte'] = null;

      $entreprise = new Entreprise();

      $form = $this->createFormBuilder($entreprise)
        ->add('nom', 'text')
        ->add('activite', 'text')
        ->add('adresse', 'text')
        ->add('siteweb', 'text', array('label' => 'Site Web'))
        ->add('submit', 'submit', array(
            'label' => 'Ajouter',
            'attr'  => array('class' => 'btn btn-primary center-block')
        ))
        ->getForm();

      $form->handleRequest($request);

      if ($form->isValid()) {

        $nom = $form->getData()->getNom();

        $entrepriseExistant = $this
          ->getDoctrine()
          ->getManager()
          ->getRepository('prostagesPropositionsBundle:Entreprise')
          ->findOneByNom($nom);

        if(!is_object($entrepriseExistant)) {

          $em = $this
            ->getDoctrine()
            ->getManager();

          $em->persist($entreprise);
          $em->flush();

          $log['etat'] = true;
          $log['texte'] = "L'entreprise $nom a bien été ajouté";

        } else {

          $log['etat'] = false;
          $log['texte'] = "L'entreprise $nom est déja existante";

        }

      }

      return $this->render('prostagesPropositionsBundle:Propositions:form.html.twig', array(
        'titre' => 'Ajouter une entreprise',
        'form' => $form->createView(),
        'log' => $log,
      ));

    }

    public function ajouterFormationAction(Request $request) {

      $log['etat'] = null;
      $log['texte'] = null;

      $formation = new Formation();

      $form = $this->createFormBuilder($formation)
        ->add('nom', 'text')
        ->add('submit', 'submit', array(
            'label' => 'Ajouter',
            'attr'  => array('class' => 'btn btn-primary center-block')
        ))
        ->getForm();

      $form->handleRequest($request);

      if ($form->isValid()) {

        $nom = $form->getData()->getNom();

        $formationExistant = $this
          ->getDoctrine()
          ->getManager()
          ->getRepository('prostagesPropositionsBundle:Formation')
          ->findOneByNom($nom);

        if(!is_object($formationExistant)) {

          $em = $this
            ->getDoctrine()
            ->getManager();

          $em->persist($formation);
          $em->flush();

          $log['etat'] = true;
          $log['texte'] = "La formation $nom a bien été ajouté";

        } else {

          $log['etat'] = false;
          $log['texte'] = "La formation $nom est déja existante";

        }

      }

      return $this->render('prostagesPropositionsBundle:Propositions:form.html.twig', array(
        'titre' => 'Ajouter une formation',
        'form' => $form->createView(),
        'log' => $log,
      ));
    }

    public function ajouterStageAction(Request $request) {

      $log['etat'] = null;
      $log['texte'] = null;

      $stage = new Stage();

      $form = $this->createFormBuilder($stage)
        ->add('nom', 'text')
        ->add('mail', 'text')
        ->add('mission', 'textarea')
        ->add('formations', 'entity', array(
              'class' => 'prostagesPropositionsBundle:Formation',
              'choice_label' => 'nom',
              'multiple' => true,
              'expanded' => true,
            ))
        ->add('entreprise', 'entity', array(
              'class' => 'prostagesPropositionsBundle:Entreprise',
              'choice_label' => 'nom',
            ))
        ->add('submit', 'submit', array(
              'label' => 'Ajouter',
              'attr'  => array('class' => 'btn btn-primary center-block')
            ))
        ->getForm();

      $form->handleRequest($request);

      if ($form->isValid()) {

          $em = $this
            ->getDoctrine()
            ->getManager();

          $em->persist($stage);
          $em->flush();

          $log['etat'] = true;
          $log['texte'] = "Le stage a bien été ajouté";

          $nom = $form->getData()->getNom();

      }

      return $this->render('prostagesPropositionsBundle:Propositions:form.html.twig', array(
        'titre' => 'Ajouter un stage',
        'form' => $form->createView(),
        'log' => $log,
      ));
    }
}
